# Rust (C#)
After finishing the web installation you can proceed with the C# plugin installation.

## Creating the server
Within Ember's admin panel, [set up a new Rust server](../configuration/server.md). Note the token of the server you just created.

## Uploading the plugin
Download the plugin from [uMod](https://umod.org/plugins/ember). Move the `ember.cs` file to the `oxide/plugins` directory of your Rust server.

## Configuration file
(Re)start your Rust server. Open the generated `oxide/config/ember.json` file for editing. Insert the URL of your Ember installation and the token of the server you created earlier. The token is used for granting the correct donation packages and storing bans, so make sure it is correct.

## Verifying the connection
Reload the plugin. Inspect the server's console. Provided that the plugin is properly configured a success message from Ember should show up.