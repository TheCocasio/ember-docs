# Garry's Mod (Lua)
After finishing the web installation you can proceed with the Lua addon installation.

## Creating the server
Within Ember's admin panel, [set up a new Garry's Mod server](../configuration/server.md). Note the token of the server you just created.

## Uploading the files
Open up the extracted archive. Navigate to the `lua` subdirectory. Move the `ember` subdirectory to the `addons` directory of your Garry's Mod server.

## Configuration file
Open the `lua/ember_config.lua` file for editing. Insert the URL of your Ember installation and the token of the server you created earlier. The token is used for granting the correct donation packages and storing bans, so make sure it is correct.

## Verifying the connection
(Re)start your server. As it's starting, inspect the server's console. Provided that the addon is properly configured a success message from Ember should show up.