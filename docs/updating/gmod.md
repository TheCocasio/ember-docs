# Garry's Mod (Lua)
After finishing the web update you can proceed with the Lua addon update.

## Removing the old files
Prior to updating, remove old files that you haven't modified to ensure a clean installation. As in, everything but `lua/ember_config.lua`.

## Downloading the new files
Download the new files from [GmodStore](https://www.gmodstore.com/scripts/view/5620). Take a look at `lua/ember_config.lua` to see if new settings have been introduced since the last update. If so, copy the new settings into your existing `lua/ember_config.lua` file.

## Applying the update
Move the new files to your Garry's Mod server's `addons` directory.
