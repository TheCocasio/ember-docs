# Troubleshooting
Here you can find some common issues and steps for troubleshooting them.

## 500 internal server error
An internal server error might occur for a number of reasons. To determine the cause more detailed logs are needed. Fetch them using the instructions below. You can use them to debug the issue yourself or attach them to a support ticket.
* check `storage/logs/app.log`
* set `development_mode` from `config.php` to `true` and reload the page
  * if the error message doesn't change, try checking your PHP logs directly
* check your web server logs (they're usually located somewhere in `/var/log`. On managed hosting there's typically a control panel option for viewing them)

## Server offline
If server cards on the landing page are displaying "Offline" instead of the "Join" button, make sure that
* the game server is on and listening to [SRCDS queries](https://developer.valvesoftware.com/wiki/Server_queries)
* you've specified the correct IP and query port for the [server](/configuration/server)
* you have enabled `allow_url_fopen` from [php.ini](https://www.php.net/manual/en/configuration.file.php)
* there's no firewall blocking the outbound UDP connection

## Payments not appearing
If users are not receiving credits or packages after a successful payment, check for any error messages under **Admin** > **Store** > **Logs** > **Failed payment logs**. If nothing shows up, depending on your payment processor check the following
* [PayPal IPN history](https://www.paypal.com/cgi-bin/webscr?cmd=_display-ipns-history)
* or [Stripe webhook](https://dashboard.stripe.com/webhooks) logs
